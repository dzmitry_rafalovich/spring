package by.shag.rafalovich;

import by.shag.rafalovich.jpa.R2D2;
import by.shag.rafalovich.jpa.RobotShop;
import by.shag.rafalovich.jpa.T1000;
import by.shag.rafalovich.jpa.Terminator;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("db.properties")
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;


    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
        System.out.println("!!!!!!!!!!");
        //RobotShop robotShop = (RobotShop) context.getBean("robotShop1");
        RobotShop robotShop = context.getBean(RobotShop.class);
        robotShop.show("T1000");
        robotShop.show("terminator");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
