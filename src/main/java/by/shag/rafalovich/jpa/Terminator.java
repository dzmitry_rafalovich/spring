package by.shag.rafalovich.jpa;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("arni")
public class Terminator implements Robot {

    @Value("${terminator.name}")
    private String name;

    @Override
    public void sayName() {
        System.out.println("I am " + name);
    }
}
