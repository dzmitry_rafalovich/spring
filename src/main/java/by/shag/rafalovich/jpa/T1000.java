package by.shag.rafalovich.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

//@Component
@Service
//@Repository
//@Controller
public class T1000 implements Robot {

    @Value("${t1000.name}")
    private String name ="T-1000";

    public void sayName() {
        System.out.println("I am " + name);
    }
}
