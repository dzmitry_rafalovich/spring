package by.shag.rafalovich.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

//@Component()
public class RobotShop {

    private String name;

    @Qualifier("t1000")
    @Autowired
    private Robot t1000;

    @Qualifier("arni")
    @Autowired(required = false)
    private Robot terminator2;

//    @Autowired
    private R2D2 r2D2;


    public void show(String name) {
        if ("T1000".equals(name)) {
            t1000.sayName();
        } else {
            terminator2.sayName();
        }
    }

    public void setName(String name) {
        this.name = name;
    }
}
